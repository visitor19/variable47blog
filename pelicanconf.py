#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'variable47'
SITENAME = u'variable47'
SITEURL = ''

PATH = 'content'

PLUGIN_PATHS = ['plugins']
PLUGINS = ['better_figures_and_images','render_math']
RESPONSIVE_IMAGES = True
STATIC_PATHS = ['images','img','pdfs']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'English'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Stackoverflow Pelican', 'https://stackoverflow.com/questions/tagged/pelican'),
         ('Jinja2', 'http://jinja.pocoo.org/'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),)

DEFAULT_PAGINATION = 30

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
