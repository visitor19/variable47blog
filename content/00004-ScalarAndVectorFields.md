Title: Scalar and Vector Fields
Date: 2016-01-01 10:33
Category: Applied-Maths 
Tags: applied-maths, fields 
Slug: Scalar and Vector Fields-
Author: Variable47
Summary: This post reviews mathematical theory for the topics of Scalar and Vector fields. 

This post reviews mathematical theory for the topics of Scalar and Vector fields. This post will be updated over time.   

Topics Covered   

```
- Gradient Vector and maximum slope
- Review: Taylor Polynomials and the Hessian matrix
- Minima, maxima and saddle points
- Area Integrals using Cartesian, Cylindrical and Spherical coordinate systems 
- Surface Integrals
- Gradient of a vector field
- Divergence of a vector field 
- Curl of a vector field
- Line integrals of scalar fields
- Line integrals of vector fields
- Line integral of gradient fields
- Flux and the divergence theorem
- Circulation and the curl theorem 
```

Introduction 

$\alpha = 9^2$
$\frac{du}{dt}$ and $