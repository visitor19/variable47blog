Title: Test Analysis 
Date: 2017-12-15 9:33
Category: Software Testing  
Tags: SoftwareTesting  
Slug: Test Analysis
Author: Variable47
Summary: Test Analysis and Design

ISEB/ISTQB define Analysis as being concerned with test conditions and combining these such that a minimal number of test cases can cover as many test conditions as possible. 