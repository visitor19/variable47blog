Title: Camping in Brecon, Wales
Date: 2011-11-01 9:33
Category: Journal  
Tags: Photos
Slug: Brecon2011  
Author: Variable47
cover: /images/Brecon2011Summary.png
Summary: Camping in Brecon, Wales. 

A selection of photos made while camping in Brecon, Wales. All photos taken with Lumix Lx3.  
  
![CampingWales2011p1]({filename}/images/CampingWales2011p1.png "CampingWales2011p1")
![CampingWales2011p2]({filename}/images/CampingWales2011p2.png "CampingWales2011p2")
![CampingWales2011p3]({filename}/images/CampingWales2011p3.png "CampingWales2011p3")
![CampingWales2011p4]({filename}/images/CampingWales2011p4.png "CampingWales2011p4")
![CampingWales2011p5]({filename}/images/CampingWales2011p5.png "CampingWales2011p5")
![CampingWales2011p6]({filename}/images/CampingWales2011p6.png "CampingWales2011p6")
![CampingWales2011p7]({filename}/images/CampingWales2011p7.png "CampingWales2011p7")
![CampingWales2011p8]({filename}/images/CampingWales2011p8.png "CampingWales2011p8")
![CampingWales2011p9]({filename}/images/CampingWales2011p9.png "CampingWales2011p9")
![CampingWales2011p10]({filename}/images/CampingWales2011p10.png "CampingWales2011p10")
![CampingWales2011p11]({filename}/images/CampingWales2011p11.png "CampingWales2011p11")
![CampingWales2011p12]({filename}/images/CampingWales2011p12.png "CampingWales2011p12")
![CampingWales2011p13]({filename}/images/CampingWales2011p13.png "CampingWales2011p13")
![CampingWales2011p14]({filename}/images/CampingWales2011p14.png "CampingWales2011p14")
![CampingWales2011p15]({filename}/images/CampingWales2011p15.png "CampingWales2011p15")
![CampingWales2011p16]({filename}/images/CampingWales2011p16.png "CampingWales2011p16")
![CampingWales2011p17]({filename}/images/CampingWales2011p17.png "CampingWales2011p17")
![CampingWales2011p18]({filename}/images/CampingWales2011p18.png "CampingWales2011p18")
![CampingWales2011p19]({filename}/images/CampingWales2011p19.png "CampingWales2011p19")
![CampingWales2011p21]({filename}/images/CampingWales2011p21.png "CampingWales2011p21")
![CampingWales2011p22]({filename}/images/CampingWales2011p22.png "CampingWales2011p22")
