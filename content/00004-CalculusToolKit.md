Title: Calculus Tool Kit 
Date: 2017-10-10 14:33
Category: Applied-Maths 
Tags: maths,applied-maths
Slug: Calculus_ToolKit
Author: Variable47
Summary: Various calculus examples and notes. Also covers some pre calc topics. 
Latex:

Various calculus examples and notes. Also covers some pre-calc topics.  This post will be updated over time. 

Topics Covered   

	- U-Substitution Examples 
	- Integration by Parts Examples
	- Inequalities 
	- Exponentials 
	- Logarithms
	- Solving Logarithmic equations
	- Complex Numbers 
	- Transposing between Exponential and Complex form. 
	- Finding the equation of the tangent to a curve at a particular point. 
	- Stationary points and the second derivative test. 


####U-Substitution Examples

Where direct integration isn't possible it may be possible to perform a substitution and then integrate. Rewriting the entire integral in terms of u by noting the integral is a composition of a function and it's derivative. u-substitution can be thought of as the reverse chain rule. 

#####Example-1

Case: Straight forward application using power rule. 

#####Example-2

Case: Substitution requires manipulation before integration.  Need to rearrange du expression. 

$\int \frac{x^3}{\sqrt{1-x^4}} dx$ let $u = 1-x^4$ then $du = -4x^3 dx$. We want the du to match exactly what is left over after u is chosen in the original, in this case $x^3$. Currently it does not because of the $-4$ coefficient. Dividing both sides of $du = -4x^3 dx$ by -4 gives $\frac{du}{-4} = \frac{-4x^3}{-4} dx$ $\dashrightarrow$ $\frac{du}{-4} = {x^3}dx$ $\dashrightarrow$ $-\frac{1}{4} du = {x^3}dx$. Performing the substitution yields $ \int \frac{-\frac{1}{4}du}{\sqrt{u}} dx$ which (pulling the constant term out in front of the integral) can be written as $ -\frac{1}{4}\int \frac{-\frac{1}{4}du}{\sqrt{u}}$  $\dashrightarrow$ $ -\frac{1}{4}\int \frac{-\frac{1}{4}du}{u^\frac{1}{2}} du$ $\dashrightarrow$ $ -\frac{1}{4}\int u^\frac{-1}{2} du$ $\dashrightarrow$ noting $-\frac{1}{2} +1 = -\frac{1}{2} +\frac{2}{2} = \frac{1}{2}$ $\dashrightarrow$ $-\frac{1}{4} \frac{u^\frac{1}{2}}{\frac{1}{2}}+C$ simplifying gives $-\frac{1}{4}(2)u^\frac{1}{2}+C$ back substituting gives a final answer as $-\frac{1}{2}(1-x^4)^\frac{1}{2}+C$  $\blacksquare$

#####Example-3

Case: Substitution requires manipulation before integration. Need to rearrange u expression. 

$\int x\sqrt{x+2}  $   $dx$ let $u = x+2$ then $du = dx$ 



#####Example-3

Case: Trig function.  

$$\int \frac{sin x}{cos^3 x}dx$$



Example-4

$\int 4xe^{5x^2} dx$  let $u = 5x^2$ then $du = 10x dx$ 

$\frac{1}{10} du = x dx$

$\frac{4}{10} du = 4x dx$

$\frac{2}{5} \int e^u du$

$\therefore \int 4xe^{5x^2} dx = \frac{2}{5} e^{5x^2}+C$



####Integration by Parts Examples

if $u$ and $v$ are functions of $x$ and have continuous derivatives then $\int u dv = uv - \int v du $. In cases where u-substitution does not provide an expression which can be integrated integration by parts may possibly help.  

The rule for integration by parts is derived from the product rule for differentiation. Therefore integration by parts is useful for integrating integrands involving products of algebraic expressions as well as transcendental functions. 

$$\frac{d}{dx}[f(x)g(x)] = f(x)g'(x)+g(x)f'(x)$$

Integrating both sides;

$$f(x)g(x) = \int( f(x)g'(x)+g(x)f'(x)) dx$$

$$f(x)g(x) = \int f(x)g'(x) dx+ \int g(x)f'(x) dx$$

Rearranging; 

$$-\int f(x)g'(x) dx = -f(x)g(x) + \int g(x)f'(x) dx$$

Multiplying through by $-1$;

$$\int f(x)g'(x) dx = f(x)g(x) - \int g(x)f'(x) dx$$ 

Now let $u = f(x)$ then $du = f'(x) dx$ and let $v = g(x)$ then $dv=g'(x)dx$

$\therefore \int u dv = uv - \int v du  $     $\blacksquare$

Choice of $u$ and $dv$

1. Try allowing $dv$ to equal the most complicated portion of the integrand that fits a basic integration rule. 
2. Try allowing $u$ to equal the factor of the integrand whose derivative is simpler than $u$. 

Note the mnemonic $L.I.A.T.E.$  to determine choice of $u$ as the factor which corresponds to the earliest letter in the mnemonic working from left to right. $Logarithms$ , $Inverse Trigonometric functions$, $Algebraic functions$ , $Trigonometric Functions$ , $Exponential Functions$ (the last two are interchangeable). 













$$\int x e^x dx$$



#### Exponentials

The Taylor series for exp(x) is given as $exp(x) = 1 + x + \frac{x^2}{2!}+ \frac{x^3}{3!}+...+ \frac{x^n}{n!}+...$

where $n! = n(n-1)(n-2)*...*3*2*1$

We note the following interesting properties of exp(x)

1. $$\frac{exp(x)}{dx}=exp(x)$$
2. $$exp(x+y) = exp(x)*exp(y)$$



- $e^x > 0$
- $$\frac{e^x}{e^y} = e^{x-y}$$
- $$e^x*e^y = e^{x+y}$$
- $$(e^x)^y = e^{xy}$$



Writing exponentials of the form $exp(x)$ using Euler's number, $e$. 

$$e = exp(1) = 2.718281828...$$



$ y = a^x,    a>0$    e.g.    $6^\pi$

$y = exp(\ln(a)) = e^{\ln a}$

$a^x = e^{kx} $ where $k = \ln a$







#### Complex Numbers

Let $z = a + bi$ where $z=\sqrt{-1}$  then, the complex conjugate $z^*$ is formed as $z^* = a-bi$ and the modulus $|z|$ as $|z| = \sqrt{a^2+b^2}$. The real part of $z$ is given by $Re(z) = a$ and the imaginary part as $Im(z) = b$ 



| Cartesian $\rightarrow$ Polar      $r \neq 0$ | Polar $\rightarrow$ Cartesian |
| ---------------------------------------- | ----------------------------- |
| $r = \sqrt{x^2+y^2}$                     | $$y= r sin \theta$$           |
| $$cos  \theta = \frac{x}{r}$$            | $$x = r cos \theta$$          |
| $$sin \theta = \frac{y}{r}$$             |                               |

Polar form:

$$z=x+yi=x+iy=r\cos\theta + ir\sin\theta = r(\cos\theta+i\sin\theta)$$

Noting $$\theta$$ is not unique. There is however a unique value of $$\theta$$ in the range $$-\pi<\theta\leq\pi$$. This is denoted the principle value of the argument, $$Arg(z)$$.

For $$x \in \mathbb{R}$$ Euler's formula states: $exp(ix)=\cos x + i \sin x$ $$\equiv$$ $$e^{ix} = \cos x + i \sin x$$

$$\therefore$$ any written as number can be $$z = x+iy \equiv re^{i\theta}$$.





######Addition, Subtraction and Multiplication 

As per real numbers. 

###### Division 

If $u$ and $v$ are both complex numbers then $$\frac{u}{v}$$ is obtained by multiplying numerator and denominator by the complex conjugate of the of the denominator, $v^*$.  This process will always reduce the denominator to a real number due to the fact that the product of a complex number and it's conjugate is always real. 

$$\frac{u}{v} = \frac{uv^*}{vv^*} = \frac{uv^*}{|v|^2} = \frac{(a+bi)(c-di)}{c^2+d^2}$$

noting $|z| = \sqrt{a^2+b^2}$ therefore $|z|^2 = a^2+b^2$ 





$$z = x + iy = r exp(i\theta)$$

de Moivre's theorem states that for a complex number of unit modulus ($$r=1$$) then $$(\cos \theta +i \sin \theta)^n = \cos (n\theta)+i\sin(n\theta)$$.













$\alpha = 9^2$

$\frac{du}{dt}^x$ and $\frac{d^2 u}{dx^2}$