Title: Hello 
Date: 2018-06-01 8:33
Category: About 
Tags: Coding
Slug: Automation
Author: Variable47
Summary: An overview of tools used in test automation. 
Latex:

Cucumber is a test automation framework which supports Behaviour Driven Development (BDD). An applications behaviour is defined as one or more scenarios within a Cucumber feature file using an English language format called Gherkin.  Glue code links a scenario to executable code. In Java this is done via the Step Definition method.  

Cucumber Keywords:	Feature; Scenario; Given; When; Then; And; But. 







This blog has been created using Pelican, a static site generator written in Python. Unlike a Content Management System(CMS) such as Wordpress, a static site 
