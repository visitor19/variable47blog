Title: A month in India 
Date: 2017-02-10 9:33
Category: Journal  
Tags: Travel
Slug: India-Jan2k17   
Author: Variable47
cover: /images/India-Jan2k17Summary.png
Summary: A month in India. Traveling through Goa, Kerela and Hampi. 

A month in India. Traveling through Goa, Kerela and Hampi. This was my second visit to India. In 2015 I spent two weeks, Starting in Mumbai and travelling by train to Rishikesh, in between reaching this destination I visited Delhi, Agra and Jaipur. On this trip I wanted to visit the south of India, I had heard lots about Goa, Kerela and Hampi. 


![India01]({filename}/images/India01.png "India01")
![India02]({filename}/images/India02.png "India02")
![India03]({filename}/images/India03.png "India03")

![India04]({filename}/images/India04.png "India04")
![India05]({filename}/images/India05.png "India05")
![India06]({filename}/images/India06.png "India06")

![India07]({filename}/images/India07.png "India07")
![India08]({filename}/images/India08.png "India08")
![India09]({filename}/images/India09.png "India09")

![India10]({filename}/images/India10.png "India10")
![India11]({filename}/images/India11.png "India11")
![India12]({filename}/images/India12.png "India12")

![India13]({filename}/images/India13.png "India13")
![India14]({filename}/images/India14.png "India14")
![India15]({filename}/images/India15.png "India15")

![India16]({filename}/images/India16.png "India16")
![India17]({filename}/images/India17.png "India17")
![India18]({filename}/images/India18.png "India18")

![India19]({filename}/images/India19.png "India19")
![India20]({filename}/images/India20.png "India20")
![India21]({filename}/images/India21.png "India21")

![India22]({filename}/images/India22.png "India22")
![India23]({filename}/images/India23.png "India23")
![India24]({filename}/images/India24.png "India24")

![India25]({filename}/images/India25.png "India25")
![India26]({filename}/images/India26.png "India26")
![India27]({filename}/images/India27.png "India27")









