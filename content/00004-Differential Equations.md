Title: Differential Equations
Date: 2017-10-10 14:33
Category: Applied-Maths 
Tags: maths,applied-maths
Slug: DifferentialEquations
Author: Variable47
Summary: This post reviews analytic techniques for classifying and solving first and second order differential equations and partial differential equations. 
Latex:

This post reviews analytic techniques for classifying and solving first and second order differential equations and partial differential equations. This post will be updated over time. 

Topics Covered   

	- Homogenous and Inhomogeneous First Order Differential Equations
	- General and Particular Solutions, Initial Value Problems arising from Initial Conditions 
	- Direct Integration 
	- Seperation of Variables 
	- Integrating Factor Method for first order linear differential equations
	- Linearity and the Principle of superposition 
	- Auxiliary equation of homogeneous linear constant-coefficient 2nd-order differential equations
	- General solution of homogeneous constant-coefficient 2nd-order differential equation
	- General solution of inhomogeneous constant-coefficient 2nd-order differential equation
	- Method of undetermined coefficients
	- Initial conditions and boundary conditions (Initial-value and Boundary-value problems) 
	- Seperation of Variables for Partial Differential Equations 
	- Wave Equation
	- Diffusion / Heat Equation 
	- Non Linear differential equations
	- Lotka-Volterra Equations 
	- Equilibrium Points 
	- Classification of Equilibrium Points
	- Equations of motion for a rigid pendulum

Introduction 

$\alpha = 9^2$
$\frac{du}{dt}$ and $\frac{d^2 u}{dx^2}$