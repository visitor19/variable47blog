Title: Statistics | Modelling Random Variables and Probability Models 
Date: 2016-01-01 9:50
Category: Statistics-Maths 
Tags: Statistics
Slug: StatisticsRandomVariables
Author: Variable47
Summary: Modelling Random Variables and Probability Models 
Latex:

This post will be updated over time.   
 
Topics Covered   

	- Random Variables 
	- Probability Mass Function (pmf) and Probability Densitity Function (pdf)
	- Bernoulli probability distribution 
	- Geometric probability distribution 
	- Poisson probability distribution
	- Discrete uniform distribution
	- Continuous uniform distribution
	- Standard uniform distribution
	- Population mean for continuous and discrete distributions
	- Population varience for continuous and discrete distributions
	- Exponetial distribution 
	- Bernoulli and Poisson Processes 
	- Population quantities
	

Introduction 

A random variable can arise from a discrete source (such as an integer count) or a continuous source (such as from direct measurement of a physical data source).

Taking each possible value of a random variable and linking it with it's probability of occurrence yields a probability distribution. Representing the outcome of a study by a random variable allows us to express the outcomes using a mathematical function. This probability function is called the probability mass function p.m.f when considering discrete random variables and a probability density function p.d.f when considering continuous random variables. 

The cumulative distribution function 

$\alpha = 9^2$
$\frac{du}{dt}$ and $\frac{d^2 u}{dx^2}$