Title: Three Weeks in Scotland,Orkney and Shetland 
Date: 2017-10-16 9:33
Category: Journal  
Tags: Travel
Slug: Scotland-AugSept2k17   
Author: Variable47
cover: /images/Shetland16Summary.png
Summary: Three Weeks in Scotland,Orkney and Shetland 

At the end of August I decided to combine a trip to the Edinburgh fringe comedy festival with a drive around Scotland. There was no huge plan. I'd heard of
the North West 500, Scotlands answer to the American Route 50. I also knew I'd like to make it to John O Groats, the most notherly point in the UK. Friends who have travelled in Scotland mentioned Wild Camping (it's legal to camp anywhere in Scotland unlike the rest of the United Kingdom), Bothy's, Midges and locations such as Aviemore and Thurso. 

My car is a 2001 Celica which I bought in 2012 after returning from working in the United States. It's not a fuel efficient car for a daily commute but I figured
if I was going to be doing a daily 80 mile round trip for work I'd want to enjoy the ride. Despite having to fork out a fare bit of money over the years to keep
it running (Toyota's are supposed to be very reliable?) I absolutely love this car. It feels like being in a fighter jet or a spaceship and when I'm driving like an extension of my body. I know intuitively where all the controls are, the car fits like a glove. It's quite a low seating position, and it can be deceptively resposive. I only realise how much I love driving this car when I have to spend time in hire cars. In contrast I often find them like driving a Tractor, slow, clunky with the responsiveness of a huge ship. Regardless, I was somewhat anxious that the car may break at some point and I wanted to have at least thought through likely scenarios. I packed blankets and coats, torches and first aid. I made sure I knew where the wheel locks were and printed all the relevent breakdown and insurance paperwork and put them in the glove compartment. The things I didn't do but felt I should have were to store an empty jerry can in the boot along with jump cables and a car jack. 

My camping equipment included a Vangaurd 2 person tent which I had not tested before travel! A Rab sleeping bag, groundmap, stove, half a dozen food ration packs and a whisky flash. For entertainment I brought my spanish tuition cds for the car (I figured traveling and learning Spanish would be a great combination) and an Irish tin whisle for camping. 

I set off from Bristol just before 3pm and headed straight up the M5/M6 North towards Edinburgh. I made a number of service station rest stops on the way and arrived around 11pm. My first night on the road concluded with a pint of Guinness in a bar chatting to people from Ireland who had travelled to Scotland for the
comedy festival.  

![Edinburgh01]({filename}/images/Edinburgh01.png "Edinburgh01")
![Edinburgh02]({filename}/images/Edinburgh02.png "Edinburgh02")
![Edinburgh03]({filename}/images/Edinburgh03.png "Edinburgh03")

![BenNevis01]({filename}/images/BenNevis01.png "BenNevis01")
![BenNevis02]({filename}/images/BenNevis02.png "BenNevis02")
![BenNevis03]({filename}/images/BenNevis03.png "BenNevis03")

![GlenCoe01]({filename}/images/GlenCoe01.png "GlenCoe01")
![GlenCoe02]({filename}/images/GlenCoe02.png "GlenCoe02")
![GlenCoe03]({filename}/images/GlenCoe03.png "GlenCoe03")

![Skye01]({filename}/images/Skye01.png "Skye01")
![Skye02]({filename}/images/Skye02.png "Skye02")
![Skye03]({filename}/images/Skye03.png "Skye03")

![Skye04]({filename}/images/Skye04.png "Skye04")
![Skye05]({filename}/images/Skye05.png "Skye05")
![Skye06]({filename}/images/Skye06.png "Skye06")

![Skye07]({filename}/images/Skye07.png "Skye07")
![Skye08]({filename}/images/Skye08.png "Skye08")
![Skye09]({filename}/images/Skye09.png "Skye09")

![Skye10]({filename}/images/Skye10.png "Skye10")
![Skye11]({filename}/images/Skye11.png "Skye11")
![Skye12]({filename}/images/Skye12.png "Skye12")

![Skye13]({filename}/images/Skye13.png "Skye13")
![Skye14]({filename}/images/Skye14.png "Skye14")
![RogieFalls01]({filename}/images/RogieFalls01.png "RogieFalls01")

![Camping01]({filename}/images/Camping01.png "Camping01")
![Wick01]({filename}/images/Wick01.png "Wick01")
![John0Groats01]({filename}/images/John0Groats01.png "John0Groats01")

![Orkney01-ScapperFlow]({filename}/images/Orkney01-ScapperFlow.png "Orkney01-ScapperFlow")
![Orkney02]({filename}/images/Orkney02.png "Orkney02")
![Orkney03]({filename}/images/Orkney03.png "Orkney03")

![Orkney04]({filename}/images/Orkney04.png "Orkney04")
![Orkney05]({filename}/images/Orkney05.png "Orkney05")
![Orkney06]({filename}/images/Orkney06.png "Orkney06")

![Orkney07]({filename}/images/Orkney07.png "Orkney07")
![Orkney08]({filename}/images/Orkney08.png "Orkney08")
![Orkney09]({filename}/images/Orkney09.png "Orkney09")

![Shetland01]({filename}/images/Shetland01.png "Shetland01")
![Shetland02]({filename}/images/Shetland02.png "Shetland02")
![Shetland03]({filename}/images/Shetland03.png "Shetland03")

![Shetland04]({filename}/images/Shetland04.png "Shetland04")
![Shetland05]({filename}/images/Shetland05.png "Shetland05")
![Shetland06]({filename}/images/Shetland06.png "Shetland06")

![Shetland07]({filename}/images/Shetland07.png "Shetland07")
![Shetland08]({filename}/images/Shetland08.png "Shetland08")
![Shetland09]({filename}/images/Shetland09.png "Shetland09")

![Shetland10]({filename}/images/Shetland10.png "Shetland10")
![Shetland11]({filename}/images/Shetland11.png "Shetland11")
![Shetland12]({filename}/images/Shetland12.png "Shetland12")

![Shetland13]({filename}/images/Shetland13.png "Shetland13")
![Shetland14]({filename}/images/Shetland14.png "Shetland14")
![Shetland15]({filename}/images/Shetland15.png "Shetland15")

![Shetland16]({filename}/images/Shetland16.png "Shetland16")
![Shetland17]({filename}/images/Shetland17.png "Shetland17")
![Shetland18]({filename}/images/Shetland18.png "Shetland18")


















