Title:Satellite Orbit Tracking
Date: 2017-02-10 9:33
Category: Projects  
Tags: Dev
Slug: OrbitTracking   
Author: Variable47
Summary: A reimplementation of a SGP4 Orbital Propagator 

In 2008 as part of my MSc Project I implemented a SGP4 Satellite Tracking Application using Matlab. It took considerable time and effort mainly due the need to 
write a graphical user interface in Matlab. At that time interoprability between Matlab and other software languages such as Java and Python was in it's infancy. As technology evolved (in particular the Maths and Science libraies available in Python) I've been keen to take some time to write a native open source application for Satellite tracking. Ofcourse many exist these days but I'm curious to see for myself how much simplier and quicker it will be to implement.