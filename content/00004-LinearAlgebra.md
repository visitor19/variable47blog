Title: Linear Algebra
Date: 2016-01-01 9:33
Category: Applied-Maths 
Tags: maths, applied-maths, linear-algebra  
Slug: LinearAlgebra
Author: Variable47
Summary: This post reviews mathematical theory for the topic of Linear Algebra and Vector Spaces. 
Latex:

This post reviews mathematical theory for the topic of Linear Algebra  and Vector Spaces. This post will be updated over time.  

Topics Covered   

	- Gaussian Elimination
	- Eigenvalues and Eigenvectors
	- Vector Spaces
	- Systems of linear differential equations (first order systems) 
	- Systems of linear differential equations (2nd order systems) 
	- Normal modes

Introduction 

$\alpha = 9^2$
$\frac{du}{dt}$ and $\frac{d^2 u}{dx^2}$