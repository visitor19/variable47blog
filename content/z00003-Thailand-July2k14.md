Title: Two weeks in Thailand
Date: 2014-07-30 9:33
Category: Journal  
Tags: Travel
Slug: Thailand-July2k14   
Author: Variable47
cover: /images/ThailandJuly2k14Summary.png
Summary: Two weeks in Thailand

Two weeks in Thailand visiting a friend. Landing in Bangkok and travelling to Surat Thani and onwards by boat to Koh Phanghan. Highlights included eating lots of amazing Thai food and many, many pancakes. While on Koh Phanghan I stayed on the East side of the island not far from Secret Beach and close to yoga schools, cafes and music venues while far away from the party side of the island. I also spent a few nights in Baan Tai from where we went on a four hour jungle night treck to Had Yuan.   


![Thailand01]({filename}/images/Thailand01.png "Thailand01")
![Thailand02]({filename}/images/Thailand02.png "Thailand02")
![Thailand03]({filename}/images/Thailand03.png "Thailand03")
![Thailand04]({filename}/images/Thailand04.png "Thailand04")
![Thailand05]({filename}/images/Thailand05.png "Thailand05")
![Thailand06]({filename}/images/Thailand06.png "Thailand06")


