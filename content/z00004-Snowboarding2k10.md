Title: Snowboarding France
Date: 2010-02-01 9:33
Category: Journal  
Tags: Travel
Slug: Snowboarding2k10  
Author: Variable47
cover: /images/Summary_Snowboarding2k10.png
Summary: A week Snowboarding in Morzine and Avoriaz, France.  

A week Snowboarding in Morzine and Avoriaz, France. Flying from Bristol to Geneva and then transfering by bus to Morzine. A fantastic weeks stay
at Chalet Roc Soleil run by tgski and hosted by the fabulous Sarah. All photos taken with a Lumix Lx3.   

![Snowboarding01]({filename}/images/Snowboarding01.png "Snowboarding01")
![Snowboarding02]({filename}/images/Snowboarding02.png "Snowboarding02")
![Snowboarding03]({filename}/images/Snowboarding03.png "Snowboarding03")

![Snowboarding04]({filename}/images/Snowboarding04.png "Snowboarding04")
![Snowboarding05]({filename}/images/Snowboarding05.png "Snowboarding05")
![Snowboarding06]({filename}/images/Snowboarding06.png "Snowboarding06")

![Snowboarding07]({filename}/images/Snowboarding07.png "Snowboarding07")
![Snowboarding08]({filename}/images/Snowboarding08.png "Snowboarding08")
![Snowboarding09]({filename}/images/Snowboarding09.png "Snowboarding09")
