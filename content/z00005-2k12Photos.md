Title: Lx3 Photos, 2012
Date: 2012-12-01 9:33
Category: Journal  
Tags: Photos
Slug: Lx3Photos2012  
Author: Variable47
cover: /images/Lx3Photos2012Summary.png
Summary: A selection of photos taken during the year using an Lx3. 

A selection of photos taken during the year using an Lx3. 

![2012_Lx3_01]({filename}/images/2012_Lx3_01.png "2012_Lx3_01")
![2012_Lx3_02]({filename}/images/2012_Lx3_02.png "2012_Lx3_02")
![2012_Lx3_03]({filename}/images/2012_Lx3_03.png "2012_Lx3_03")
![2012_Lx3_04]({filename}/images/2012_Lx3_04.png "2012_Lx3_04")
![2012_Lx3_05]({filename}/images/2012_Lx3_05.png "2012_Lx3_05")
![2012_Lx3_06]({filename}/images/2012_Lx3_06.png "2012_Lx3_06")
![2012_Lx3_07]({filename}/images/2012_Lx3_07.png "2012_Lx3_07")
![2012_Lx3_08]({filename}/images/2012_Lx3_08.png "2012_Lx3_08")
![2012_Lx3_09]({filename}/images/2012_Lx3_09.png "2012_Lx3_09")
![2012_Lx3_10]({filename}/images/2012_Lx3_10.png "2012_Lx3_10")
![2012_Lx3_11]({filename}/images/2012_Lx3_11.png "2012_Lx3_11")
![2012_Lx3_12]({filename}/images/2012_Lx3_12.png "2012_Lx3_12")