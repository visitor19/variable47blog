Title: Approaches to Learning
Date: 2017-12-02 9:33
Category: Journal  
Tags: Math, Journal  
Slug: Approaches_to_Learning_Dec2k17  
Author: Variable47
Summary: Thoughts on approaches to learning

If I mentally carry out the operation 9 x 5 my minds eye 'see's a symbol for 9 then briefly a symbol for multiplication followed by the symbol 5. Instantly I then see the symbol 45. I note that if I ask my self the question slowly I sometimes 'see' the '9x5' operation as written and this is replaced by the symbol 45 upon desiring the result. It might sound tedious or trivial to describe this in such detail but I believe the useful thing to recognise is that this 'calculation' is merely association of visual images. The symbols '9', 'x' and '5' associate with (or map to) the symbol '45'.  This is analogous to the mapping my brain has from Paris to France. Here my minds eye sees one of a set of equivalent symbols for Paris e.g. the word 'Paris' or the visual representation of the Eiffel tower and returns the symbol 'France'. There are intricacies in these mappings but the essential point is that there is no computation when I carry out 9x5 It is a symbol mapping operation. 

Mental Addition 

1. Learning complements (symbol mappings) 

   | Number | Map  |
   | ------ | ---- |
   | 1      | 9    |
   | 2      | 8    |
   | 3      | 7    |
   | 4      | 6    |
   | 5      | 5    |
   | 6      | 4    |
   | 7      | 3    |
   | 8      | 2    |
   | 9      | 1    |
   | 0      | 10   |

   ​

2. Complements

   How far is 37 from 100? Ans: 100 - 37 is 63. Note 



















I spent a few days at St Catherine's college, Oxford University attending the Institute of Mathematics and Applications' conference on Cryptography and Coding.  

In Oct 2017 Bristol's quantum engineering department put the bound on the quantum singularity at 50 photons. This milestone denotes the point at which quantum computing will outperform classical algorithms. 

In 1994 Shor's algorithm predicted 

Within quantum engineering research environments such as that at Bristol University there is an active drive towards a vision of the future where quantum effects are used to secure data. Pioneering work on quantum key distribution is taking place using the Open Bristol network. 

Research on cryptography is focused on 

- Lattice based encryption 
- Homomorphic encryption 
- Individual 















I was lucky enough to have the chance to attend the 5th Mathematics in defence conference hosted by the Institute of Mathematics at the Royal Military Academy Sandhurst.  There were a number of very interesting talks on topics such as target acquisition and sonar problems, statistics in the application of forensics and the the use of lanchester modelling to predict the impact of the use of intelligence methods in warfare. 

![Sandhurst_2k17_01]({filename}/images/Sandhurst_2k17_01.png "Sandhurst_2k17_01")
![Sandhurst_2k17_02]({filename}/images/Sandhurst_2k17_02.png "Sandhurst_2k17_02")
![Sandhurst_2k17_03]({filename}/images/Sandhurst_2k17_03.png "Sandhurst_2k17_03")
![Sandhurst_2k17_04]({filename}/images/Sandhurst_2k17_04.png "Sandhurst_2k17_04")
![Sandhurst_2k17_05]({filename}/images/Sandhurst_2k17_05.png "Sandhurst_2k17_05")
![Sandhurst_2k17_06]({filename}/images/Sandhurst_2k17_06.png "Sandhurst_2k17_06")