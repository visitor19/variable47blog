Title: Two weeks in India
Date: 2015-02-28 9:33
Category: Journal  
Tags: Travel
Slug: India-Feb2k15  
Author: Variable47
cover: /images/India2k15Summary.png
Summary: Two weeks in India

Two weeks in India. Starting in Mumbai and travelling by train to Rishikesh, in between reaching this destination I visited Delhi, Agra and Jaipur. Just before my visit I'd started reading Shantaram and it felt very surreal suddenly being in Colaba, Mumbai and in particular sitting in Leopold cafe. This journey featured my first overnight Indian train ride (of which I've now done several and really enjoy them), being totally overwhelmed by the immediate chaos and intensity of Delhi and contrasting that with Rishikesh which has a quiet and peaceful feel to it (aside from the incessent motorbike beeping anywhere near the bridges).  

![India2015p01]({filename}/images/India2015p01.png "India2015p01")
![India2015p02]({filename}/images/India2015p02.png "India2015p02")
![India2015p03]({filename}/images/India2015p03.png "India2015p03")

![India2015p04]({filename}/images/India2015p04.png "India2015p04")
![India2015p05]({filename}/images/India2015p05.png "India2015p05")
![India2015p06]({filename}/images/India2015p06.png "India2015p06")

![India2015p07]({filename}/images/India2015p07.png "India2015p07")
![India2015p08]({filename}/images/India2015p08.png "India2015p08")
![India2015p09]({filename}/images/India2015p09.png "India2015p09")

![India2015p10]({filename}/images/India2015p10.png "India2015p10")
![India2015p11]({filename}/images/India2015p11.png "India2015p11")
![India2015p12]({filename}/images/India2015p12.png "India2015p12")