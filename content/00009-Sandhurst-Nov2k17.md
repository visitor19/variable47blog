Title: Mathematics in Defence 
Date: 2017-11-24 9:33
Category: Journal  
Tags: Math, Journal  
Slug: Mathematics_in_Defence-Nov2k17  
Author: Variable47
Summary: I was lucky enough to have the chance to attend the 5th Mathematics in defence conference hosted by the Institute of Mathematics at the Royal Military Academy Sandhurst. 

I was lucky enough to have the chance to attend the 5th Mathematics in defence conference hosted by the Institute of Mathematics at the Royal Military Academy Sandhurst.  There were a number of very interesting talks on topics such as target acquisition and sonar problems, statistics in the application of forensics and the the use of lanchester modelling to predict the impact of the use of intelligence methods in warfare. 

![Sandhurst_2k17_01]({filename}/images/Sandhurst_2k17_01.png "Sandhurst_2k17_01")
![Sandhurst_2k17_02]({filename}/images/Sandhurst_2k17_02.png "Sandhurst_2k17_02")
![Sandhurst_2k17_03]({filename}/images/Sandhurst_2k17_03.png "Sandhurst_2k17_03")
![Sandhurst_2k17_04]({filename}/images/Sandhurst_2k17_04.png "Sandhurst_2k17_04")
![Sandhurst_2k17_05]({filename}/images/Sandhurst_2k17_05.png "Sandhurst_2k17_05")
![Sandhurst_2k17_06]({filename}/images/Sandhurst_2k17_06.png "Sandhurst_2k17_06")