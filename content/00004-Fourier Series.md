Title: Fourier Series 
Date: 2016-01-01 07:33
Category: Applied-Maths 
Tags: maths, applied-maths, fourier 
Slug: FourierSeries 
Author: Variable47
Summary: This post reviews mathematical theory for Fourier series.  

This post reviews mathematical theory for Fourier series. This post will be updated over time.   

Topics Covered   

	- Fourier series for periodic functions
	- Fourier series for odd and even functions
	- Functions defined over a finite domain
	- Gibbs phenomenon
	- Shifting the range of integration
	- Differentiating Fourier series
	- Exponential Fourier series
	- Application to differential equations

Introduction 

$\alpha = 9^2​$
$\frac{du}{dt}$ and $\frac{d^2 u}{dx^2}$