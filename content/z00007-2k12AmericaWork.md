Title: America, 2012 
Date: 2012-07-01 9:33
Category: Journal  
Tags: Photos
Slug: America2k12  
Author: Variable47
cover: /images/America2k12Summary.png
Summary: A selection of photos taken whilst working in America. All photos taken with a Lumix Lx3.   

A selection of photos taken whilst working in America. All photos taken with a Lumix Lx3. Locations include Boston MA, Washington DC; Woodstock CT and Manhattan NY.   
  
![usa1]({filename}/images/usa1.png "usa1")
![usa2]({filename}/images/usa2.png "usa1")
![usa3]({filename}/images/usa3.png "usa3")
![usa4]({filename}/images/usa4.png "usa4")
![usa5]({filename}/images/usa5.png "usa5")
![usa6]({filename}/images/usa6.png "usa6")
![usa7]({filename}/images/usa7.png "usa7")
![usa8]({filename}/images/usa8.png "usa8")
![usa9]({filename}/images/usa9.png "usa9")
![usa10]({filename}/images/usa10.png "usa10")
![usa11]({filename}/images/usa11.png "usa11")
![usa12]({filename}/images/usa12.png "usa12")
![usa13]({filename}/images/usa13.png "usa13")
![usa14]({filename}/images/usa14.png "usa14")
![usa15]({filename}/images/usa15.png "usa15")
![usa16]({filename}/images/usa16.png "usa16")
![usa17]({filename}/images/usa17.png "usa17")
![usa18]({filename}/images/usa18.png "usa18")
![usa19]({filename}/images/usa19.png "usa19")
![usa20]({filename}/images/usa20.png "usa20")
