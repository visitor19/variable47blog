Title: Quantum Mechanics 
Date: 2017-10-10 14:33
Category: Applied-Maths 
Tags: maths,applied-maths
Slug: QM_1
Author: Variable47
Summary: Notes on quantum mechanics. Latex:

When I think of the idea of a mechanical deterministic universe my mind is drawn to images of the Victorian era and lead backwards through time. I think of talks on astronomy given to the Royal Society in London, I think of great mechanical structures, clocks, bridges and steam trains. I think of figures such as Newton, Maxwell and Brunel. A world where the scientific method has brought human accomplishment to the point of being able to predict the motion of planets and describe our observable universe in a repeatable and reliable manner. In these times the satisfaction of our understanding radiated outwards from scientific discovery to permeate engineering, economics and politics. It set modern industrialised societies completely apart from more ancient cultures and indeed became synonymous with the struggle for empire. 

Growing up in England in the 1980's and 1990's I feel that at school level the world view imparted to us as children was one which was still firmly routed in this culture. Since the turn of the century the proliferation of technology into our daily lives and our psyche has I feel had a turbulent effect on this previous world view. Quantum devices are now such an integral part of our every day lives, the average person is likely to have heard of terms such as quantum cryptography and quantum computing and terms such as shrod ringers cat, quantum engagement and the double slit experiment exist in public discourse even if their meaning has not. 

The story of Quantum Mechanics starts with Niels Bohr who in 1913 proposed that Hydrogen atoms have energy levels which are quantised. I.e. The energy level that an atom takes is from a discrete set of possible energies. This is now observed for atoms, molecules, atomic nuclei and their inner constituents.  

An atom has a ground state and a number of higher energy levels. When an atom jumps from one energy state to another it releases a packet of electromagnetic radiation called a photon. 











 









Various calculus examples and notes. Also covers some pre-calc topics.  This post will be updated over time. 



























Topics Covered   

	- U-Substitution Examples 
	- Integration by Parts Examples
	- Logarithms
	- Solving Logarithmic equations
	- Inequalities 
	- Exponentials 
	- Complex Numbers 
	- Transposing between Exponential and Complex form. 
	- Finding the equation of the tangent to a curve at a particular point. 
	- Stationary points and the second derivative test. 


####U-Substitution Examples

Where direct integration isn't possible it may be possible to perform a substitution and then integrate. Rewriting the entire integral in terms of u by noting the integral is a composition of a function and it's derivative. u-substitution can be thought of as the reverse chain rule. 

#####Example-1

Case: Straight forward application using power rule. 

#####Example-2

Case: Substitution requires manipulation before integration.  Need to rearrange du expression. 

$\int \frac{x^3}{\sqrt{1-x^4}} dx$ let $u = 1-x^4$ then $du = -4x^3 dx$. We want the du to match exactly what is left over after u is chosen in the original, in this case $x^3$. Currently it does not because of the $-4$ coefficient. Dividing both sides of $du = -4x^3 dx$ by -4 gives $\frac{du}{-4} = \frac{-4x^3}{-4} dx$ $\dashrightarrow$ $\frac{du}{-4} = {x^3}dx$ $\dashrightarrow$ $-\frac{1}{4} du = {x^3}dx$. Performing the substitution yields $ \int \frac{-\frac{1}{4}du}{\sqrt{u}} dx$ which (pulling the constant term out in front of the integral) can be written as $ -\frac{1}{4}\int \frac{-\frac{1}{4}du}{\sqrt{u}}$  $\dashrightarrow$ $ -\frac{1}{4}\int \frac{-\frac{1}{4}du}{u^\frac{1}{2}} du$ $\dashrightarrow$ $ -\frac{1}{4}\int u^\frac{-1}{2} du$ $\dashrightarrow$ noting $-\frac{1}{2} +1 = -\frac{1}{2} +\frac{2}{2} = \frac{1}{2}$ $\dashrightarrow$ $-\frac{1}{4} \frac{u^\frac{1}{2}}{\frac{1}{2}}+C$ simplifying gives $-\frac{1}{4}(2)u^\frac{1}{2}+C$ back substituting gives a final answer as $-\frac{1}{2}(1-x^4)^\frac{1}{2}+C$  $\blacksquare$

#####Example-3

Case: Substitution requires manipulation before integration. Need to rearrange u expression. 

$\int x\sqrt{x+2}  $   $dx$ let $u = x+2$ then $du = dx$ 



#####Example-3

Case: Trig function.  

$$\int \frac{sin x}{cos^3 x}dx$$



####Integration by Parts Examples

In cases where u-substitution does not provide an expression which can be integrated integration by parts may possibly help. $\int u dv = uv - \int v du $ Note the mnemonic LIATE. 

$$\int x e^x dx$$





$\alpha = 9^2$

$\frac{du}{dt}^x$ and $\frac{d^2 u}{dx^2}$