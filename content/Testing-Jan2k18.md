Title: Agile Testing
Date: 2017-12-15 9:33
Category: Journal  
Tags: Math, Journal  
Slug: Agile Testing  
Author: Variable47
Summary: Agile Testing

I spent several years working for a software testing consultancy who had a key aim of their deployed consultants striving to be the trusted lieutenants of their project manager. We were trained in both ISEB software testing certifications and also Prince2 Practitioner level project management certifications. Reflecting on my experiences in the workplace,  this personification of the testing role is in my opinion a critical one for aiding project delivery success and harmony.  

Whether the project lifecycle adopted is sequential (Waterfall, V-Model,...) or iterative (Agile, Scrum,...) or a combination thereof, the testing function should map onto all stages of the the project. By this, I mean the scope and concern of testing should not be limited to activities conducted following one or more development activities. If you accept this, then many of the concerns of the test manager (and indeed test leads, analysts and engineers) are shared with the project manager. Both testing and project management roles thus best serve the project's interests when they operate supportively and in unison. 

A project manager does not typically have to become intimately knowledgeable of requirements, specifications or designs but does have to have the confidence that such project artefacts are suitable in order to mitigate risk, resource, schedule, and navigate project gates. The test team also has risk mitigation in mind but in order to scope and plan test delivery must scrutinise project artefacts at every opportunity. This allows ample opportunity for expert opinion to be conveyed to the project management regarding the current state and quality of the project as well as identifying technical risks. 

When considering Agile methodology and the Scrum framework one key differentiator is the notion of each sprint being light on documentation. I agree that it is important to document the minimal amount necessary to successfully convey and implement the task at hand but I too feel that this should not a green light to do away with documentation entirely. For if you lose documentation you potentially also lose articulated thought, review,  baseline agreement and knowledge transfer to varying extents. 

Nobody on a project should be doing work just for the sake of it, or just to keep themselves busy. This should be a key mantra of project teams everywhere though unfortunately it is not. In the real world departments often do not adequately handle efficient de resourcing and re resourcing of a key individual to a project and so in fear of losing the resource entirely it is often sadly seen as better to have them billing 100% to a project despite being of questionable productivity. I believe this is bad for individual and group morale as well as bad to a project and each incidence of it sets a terrible precedent for the next. In this and similar fashions whole forests worth of spurious documentation can be produced and paid for by a project.       

A new project is triggered (in Prince2 terminology) when a project mandate is identifies



- Agile Project 
- Scrum Project













